<?php

namespace BBWPGraphQL\Fields;

use BBWPGraphQL\Resolvers\BBContentResolver;

class BBContentField extends CustomField {
  public function __construct(string $typeName, string $fieldName)
  {
    parent::__construct($typeName, $fieldName);
  }

  protected function setConfig(): array
  {
    return [
      'description' => 'My Custom Field',
      'type' => 'BBContentType',
      'args' => [
        'useGlobalCSS' => [
          'type' => 'Bool',
          'description' => 'Include Beaver Builder\'s global styles in the page\'s CSS output. Default - false'
        ],
        'useGlobalJS' => [
          'type' => 'Bool',
          'description' => 'Include Beaver Builder\'s global javascript in the page\'s JS output. Default - false'
        ]
      ],
      'resolve' => function ($root, $args, $context, $info) {
        return (new BBContentResolver)->singleNodeResolver($root, $args, $context, $info);
      }
    ];
  }
}