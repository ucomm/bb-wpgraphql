<?php

namespace BBWPGraphQL\Fields;

interface FieldInterface {
  public function registerField();
}