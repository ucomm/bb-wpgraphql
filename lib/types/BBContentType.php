<?php

namespace BBWPGraphQL\Types;

use BBWPGraphQL\Types\CustomType;

class BBContentType extends CustomType {
  public function __construct(string $type)
  {
    parent::__construct($type);
  }

  public function getConfig(): array
  {
    return [
      'description' => 'Beaver Builder content for a post or page.',
      'fields' => [
        'html' => [
          'type' => 'String',
          'description' => 'Content rendered by Beaver Builder'
        ],
        'css' => [
          'type' => 'String',
          'description' => 'Beaver Builder CSS for a single post or page'
        ],
        'js' => [
          'type' => 'String',
          'description' => 'Beaver Builder JS for a single post or page'
        ]
      ]
    ];
  }
}