<?php

namespace BBWPGraphQL\Resolvers;

use FLBuilder;

class BBContentResolver extends CustomResolver {
  public function singleNodeResolver($root, $args, $context, $info)
  {
    $id = $root->ID;

    $useGlobalCSS = (isset($args['useGlobalCSS']) && $args['useGlobalCSS'] === true) ? true : false;

    $useGlobalJS = (isset($args['useGlobalJS']) && $args['useGlobalJS'] === true) ? true : false;

    ob_start();

    FLBuilder::render_content_by_id($id);

    $html = ob_get_clean();
    $html = preg_replace('/[\t\n]/', "", $html);

    // replace tabs and new lines with spaces. keep the spaces intact because they mean something in css
    $css = FLBuilder::render_css($useGlobalCSS);
    $css = preg_replace('/[\t\n]/', " ", $css);

    // remove tabs and new lines 
    $js = FLBuilder::render_js($useGlobalJS);
    $js = preg_replace('/[\t\n]/', "", $js);
    return [
      'html' => $html,
      'css' => $css,
      'js' => $js
    ];
  }
}