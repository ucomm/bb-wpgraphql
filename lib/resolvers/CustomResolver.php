<?php

namespace BBWPGraphQL\Resolvers;

class CustomResolver implements ResolverInterface {

  public function singleNodeResolver($root, $args, $context, $info)
  {
    return false;
  }

  public function multipleNodesResolver($root, $args, $context, $info): array
  {
    return [];
  }
}