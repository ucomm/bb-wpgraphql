<?php

return [
  // the key should match the overall namespace of the types
  'BBWPGraphQL\\Fields\\' => [
    [
      // the class name to resolve to
      'className' => 'BBContentField',
      // where to make the connection for the field.
      // the most basic (or root) connection is to the RootQuery
      'connection' => 'Page',
      // the name that will be used/displayed in a graphql playground
      'fieldName' => 'beaverBuilderContent',
    ],
  ]
];