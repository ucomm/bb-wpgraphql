<?php

return [
  // the key should match the overall namespace of the types
  'BBWPGraphQL\\Types\\' => [
    // type with a class name at the root level of the namespace
    'BBContentType',
  ],
];
