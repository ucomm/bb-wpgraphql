<?php
/*
Plugin Name: Beaver Builder WPGraphQL Extension
Description: A plugin to allow content rendered by Beaver Builder to be read by graphql
Author: UComm Web Team/Adam Berkowitz
Version: 0.0.1
Text Domain: bb-wpgraphql
*/

use BBWPGraphQL\Connections\ConnectionRegister;
use BBWPGraphQL\Connections\MyConnection;
use BBWPGraphQL\Fields\FieldRegister;
use BBWPGraphQL\Types\TypeRegister;

if (!defined('WPINC')) {
  die;
}

define('BBWPGRAPHQL_DIR', plugin_dir_path(__FILE__));
define('BBWPGRAPHQL_URL', plugins_url('/', __FILE__));

// select the right composer autoload.php file depending on environment.
if (file_exists(dirname(ABSPATH) . '/vendor/autoload.php')) {
  require_once(dirname(ABSPATH) . '/vendor/autoload.php');
} elseif (file_exists(ABSPATH . 'vendor/autoload.php')) {
  require_once(ABSPATH . 'vendor/autoload.php');
} else {
  require_once('vendor/autoload.php');
}

require 'lib/types/CustomTypeInterface.php';
require 'lib/types/CustomType.php';
require 'lib/types/TypeRegister.php';
require 'lib/types/BBContentType.php';

require 'lib/fields/FieldInterface.php';
require 'lib/fields/CustomField.php';
require 'lib/fields/BBContentField.php';
require 'lib/fields/FieldRegister.php';

require 'lib/resolvers/ResolverInterface.php';
require 'lib/resolvers/CustomResolver.php';
require 'lib/resolvers/BBContentResolver.php';


$type_register = new TypeRegister();
$preparedTypes = $type_register->setTypes();
if (count($preparedTypes) > 0) {
  $type_register->createRegistry();
}

$field_register = new FieldRegister();
$field_register->setFields();


// $connection_register = new ConnectionRegister();
// $connection_register->setConnections();
